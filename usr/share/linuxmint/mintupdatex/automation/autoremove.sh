#!/bin/sh

if [ ! -e "/var/lib/linuxmint/mintupdatex-automatic-removals-enabled" ]
then
    exit
fi

ln -s /usr/share/linuxmint/mintupdatex/automation/99-mintupdatex-temporary.pkla /etc/polkit-1/localauthority/90-mandatory.d/99-mintupdatex-temporary.pkla
echo "\n-- Automatic Removal $(date):\n" >> /var/log/mintupdatex.log
systemd-inhibit --why="Performing autoremoval" --who="Update Manager" --what=shutdown --mode=block /usr/bin/apt-get autoremove --purge -y >> /var/log/mintupdatex.log 2>&1
rm -f /etc/polkit-1/localauthority/90-mandatory.d/99-mintupdatex-temporary.pkla
